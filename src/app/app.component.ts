import { HttpClient } from '@angular/common/http';
import { Component, OnDestroy } from '@angular/core';
import { ComponentStore } from '@ngrx/component-store';
import { EMPTY, interval, NEVER, Observable, Subscription } from 'rxjs';
import {
  catchError,
  concatMap,
  delay,
  exhaustMap,
  filter,
  map,
  skip,
  switchMap,
  tap,
  withLatestFrom,
} from 'rxjs/operators';
import { Message, notNullish } from './util';

interface State {
  username?: string;

  activeRoom?: string;
  creatingRoom: boolean;
  messageSending: boolean;

  rooms: string[];
  roomsLoading: boolean;

  messages: {
    [room: string]: Message[];
  };
}

const initialState: State = {
  messages: {},
  creatingRoom: false,
  roomsLoading: false,
  rooms: [],
  messageSending: false,
};

const API = 'http://localhost:8080'; //'https://chat.infinite-playground.com';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent extends ComponentStore<State> implements OnDestroy {
  subscription = new Subscription();

  roomsLoading$ = this.select(state => state.roomsLoading);
  activeRoom$ = this.select(state => state.activeRoom ?? null);
  messages$ = this.select(
    this.select((state) => state.messages),
    this.select((state) => state.activeRoom),
    (messages, activeRoom) => {
      return activeRoom ? messages[activeRoom]?.sort((a,b) => new Date(a.timestamp).getTime() - new Date(b.timestamp).getTime()) : undefined;
    }
  );
  creatingRoom$ = this.select(state => state.creatingRoom);
  username$ = this.select(state => state.username);
  messageSending$ = this.select(state => state.messageSending);

  roomList$ = this.select((state) => state.rooms);

  selectRoom = this.updater((state, room: string) => ({
    ...state,
    activeRoom: room,
  }));

  setUsername = this.updater((state, username: string) => {
    return {
      ...state,
      username: username
    };
  })

  constructor(private http: HttpClient) {
    super(initialState);
    this.loadRoomList();

    // Poll messages
    const interval$: Observable<string> = interval(1000).pipe(
      withLatestFrom(this.select((state) => state.activeRoom)),
      map(([, activeRoom]) => {
        return activeRoom;
      }),
      filter(notNullish)
    );

    this.subscription.add(this.loadMessages(interval$));
  }

  loadMessages = this.effect<string>((trigger$) =>
    trigger$.pipe(
      exhaustMap((roomName) => {
        console.log('loadmsg');
        return this.http
          .get<Message[]>(`${API}/rooms/${roomName}/messages`)
          .pipe(
            tap((messages) => {
              this.setState(state => ({
                ...state,
                messages: {
                  ...state.messages,
                  [roomName]: messages
                }
              }))
            }),
            catchError((e) => {
              console.log(e);
              return EMPTY;
            })
          );
      })
    )
  );

  loadRoomList = this.effect((trigger$) =>
    trigger$.pipe(
      switchMap(
        (): Observable<string[]> => {
          this.setState((state) => ({
            ...state,
            roomsLoading: true,
          }));
          return this.http.get<string[]>(`${API}/rooms`).pipe(
            catchError((e) => {
              console.log(e);
              return EMPTY;
            })
          );
        }
      ),
      tap((rooms: string[]) => {
        this.setState((state) => ({
          ...state,
          rooms,
          roomsLoading: false,
          activeRoom: state.activeRoom ?? rooms[0]
        }));
      })
    )
  );

  sendMessage = this.effect<string>((trigger$) =>
    trigger$.pipe(
      withLatestFrom(this.select((state) => state.activeRoom)),
      concatMap(([message, room]) => {
        this.setState((state) => ({ ...state, messageSending: true }));
        return this.http
          .post(`${API}/rooms/${room}/messages`, {
            username: this.get().username ?? 'No username',
            message: message,
          })
          .pipe(
            tap(() => this.loadMessages(room!)),
            delay(500),
            map(() => {
              this.setState((state) => ({ ...state, messageSending: false }));
            }),
            catchError((e) => {
              console.log(e);
              return EMPTY;
            })
          );
      })
    )
  );

  createRoom = this.effect<string>((trigger$) =>
    trigger$.pipe(
      switchMap((room) => {
        this.setState((state) => ({ ...state, creatingRoom: true }));
        return this.http.post(`${API}/rooms`, { room }).pipe(
          switchMap(() => {
            this.loadRoomList();
            return this.select((state) => state.roomsLoading).pipe(
              skip(1),
              filter((loading) => !loading)
            );
          }),
          map(() => {
            this.setState((state) => ({ ...state, creatingRoom: false }));
          }),
          catchError((e) => {
            console.log(e);
            return EMPTY;
          })
        );
      })
    )
  );

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }
}
