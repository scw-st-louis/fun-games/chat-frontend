const colors = require('tailwindcss/colors');

module.exports = {
	purge: ['./src/**/*.html', './src/**/*.ts'],
  darkMode: false, // or 'media' or 'class'
  colors: {
    gray: colors.warmGray,
    red: colors.rose,
    blue: colors.indigo,
    yellow: colors.amber,
    green: colors.green,
  },
  theme: {
    extend: {},
  },
  variants: {
    extend: {},
  },
  plugins: [],
}
